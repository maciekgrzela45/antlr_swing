package tb.antlr.interpreter;

import java.util.HashMap;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	private LocalSymbols localSymbolsTable = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected void console(String text) {
    	System.out.println(text);
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer addition(Integer a, Integer b) {
		return a+b;
	}
	
	protected Integer subtraction(Integer a, Integer b) {
		return a-b;
	}
	
	protected Integer multiplication(Integer a, Integer b) {
		return a*b;
	}
	
	protected Integer integerDivision(Integer a, Integer b) throws RuntimeException {
		if(b == 0) throw new RuntimeException("Dzielenie przez zero");
		
		return a/b;
	}
	
	protected Integer moduloDivision(Integer a, Integer b) throws RuntimeException {
		if(b == 0) throw new RuntimeException("Dzielenie przez zero");
		
		return a%b;
	}
	
	protected Integer logicalAnd(Integer a, Integer b) {
		return a & b;
	}
	
	protected Integer logicalOr(Integer a, Integer b) {
		return a | b;
	}
	
	protected Integer logicalNor(Integer a, Integer b) {
		if((a | b) == 0) {
			return 1;
		}else {
			return 0;
		}
	}
	
	protected Integer logicalNand(Integer a, Integer b) {
		if((a & b) == 0) {
			return 1;
		}else {
			return 0;
		}
	}
	
	protected Integer logicalXor(Integer a, Integer b) {
		return a^b;
	}
	
	protected Integer shiftRight(Integer a, Integer b) {
		return a >> b;
	}
	
	protected Integer shiftLeft(Integer a, Integer b) {
		return a << b;
	}
	
	protected Boolean isEqual(Integer a, Integer b) {
		return a == b;
	}
	
	protected void variableDeclaration(String variableName) {
		if(symbolExists(variableName)) {
			System.out.println("Zmienna już istnieje");
		}
		
		try {
			localSymbolsTable.newSymbol(variableName);
		}catch(RuntimeException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	protected Boolean symbolExists(String variableName) {
		HashMap<String, Integer> symbolHashMap = localSymbolsTable.hasSymbolDepth(variableName);
		if (symbolHashMap == null) {
			return false;
		}
		
		return true;
	}
	
	protected Integer getValue(String variableName) throws RuntimeException {
		try {
			Integer val = localSymbolsTable.getSymbol(variableName);
			return val;
		}catch(RuntimeException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	protected Integer setValue(String variableName, Integer variableValue) throws RuntimeException {
		try {
			return localSymbolsTable.setSymbol(variableName, variableValue);
		}catch(RuntimeException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	protected void enterBlock() {
		localSymbolsTable.enterScope();
	}
	
	protected void leaveBlock() {
		localSymbolsTable.leaveScope();
	}
}
