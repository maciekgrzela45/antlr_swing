tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (
          e = expr
          | ifStatement
          | blockStatement
          | declaration
          | console
          )*
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = addition($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = subtraction($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = multiplication($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = integerDivision($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = moduloDivision($e1.out, $e2.out);}
        | ^(AND   e1=expr e2=expr) {$out = logicalAnd($e1.out, $e2.out);}
        | ^(OR    e1=expr e2=expr) {$out = logicalOr($e1.out, $e2.out);}
        | ^(NOR   e1=expr e2=expr) {$out = logicalNor($e1.out, $e2.out);}
        | ^(NAND  e1=expr e2=expr) {$out = logicalNand($e1.out, $e2.out);}
        | ^(XOR   e1=expr e2=expr) {$out = logicalXor($e1.out, $e2.out);}
        | ^(SHL   e1=expr e2=expr) {$out = shiftLeft($e1.out, $e2.out);}
        | ^(SHR   e1=expr e2=expr) {$out = shiftRight($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = setValue($i1.text, $e2.out);}
        | ID                       {$out = getValue($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
        catch [RuntimeException exception] { System.out.println(exception.getMessage());}
        
        

blockStatement
        : LSB {enterBlock();}
         ( expr | ifStatement | blockStatement | declaration | console )*
          RSB {leaveBlock();}
        ;
        
ifStatement  returns [Boolean res]
        : ^(EQUALS e1=expr e2=expr)        {$res = isEqual($e1.out, $e2.out);}
        | ^(NOT_EQUALS e1=expr e2=expr)    {$res = !(isEqual($e1.out, $e2.out));}
        ;
        
declaration
        : ^(VAR e1=ID)                      {variableDeclaration($e1.text);}
        ;
        catch [RuntimeException exception] { System.out.println(exception.getMessage());}
        
console
        : ^(CONSOLE e1=expr)               {console($e1.text+"="+$e1.out);}
        ;
        catch [RuntimeException exception] { System.out.println(exception.getMessage());}
