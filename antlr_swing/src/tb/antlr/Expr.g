grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blockStat)+ EOF!;

stat
    : expr NL -> expr
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | ifStatement NL -> ifStatement
    | CONSOLE expr NL -> ^(CONSOLE expr)
    | NL ->
    ;
    
blockStat
    : LSB (stat | blockStat)* RSB NL!
    ;
    
ifStatement
    : IF^ ifExpression THEN! blockStat END!
    ;

ifExpression
    : expr
        ( EQUALS^     expr
    |   NOT_EQUALS^ expr
    )*
    ;


expr
    : term
      ( PLUS^ term
      | MINUS^ term
      )*
    ;

term
    : logicExpr
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;
    
logicExpr
    : atom
      ( AND^  atom
      | OR^   atom
      | NOR^  atom
      | NAND^ atom
      | XOR^  atom
      | SHR^  atom
      | SHL^  atom
      )*
     ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

LP
  : '('
  ;

RP
  : ')'
  ;
  
LSB
  : '{'
  ;
  
RSB
  : '}'
  ;

PODST
  : '='
  ;

PLUS
  : '+'
  ;

MINUS
  : '-'
  ;

MUL
  : '*'
  ;

DIV
  : '/'
  ;
  
MOD
  : '%'
  ;
  
AND
  : 'AND'
  ;

OR
  : 'OR'
  ;
  
NOR
  : 'NOR'
  ;
  
NAND
  : 'NAND'
  ;
  
XOR
  : 'XOR'
  ;
  
SHR
  : 'SHR'
  ;

SHL
  : 'SHL'
  ;
  
IF
  : 'IF'
  ;

THEN
  : 'THEN'
  ;

END
  : 'END'
  ;
  
CONSOLE
  : 'console'
  | 'CONSOLE'
  ;
  
EQUALS
  : '==='
  ;
  
NOT_EQUALS
  : '!=='
  ;
  
BLOCK
  : 'BLOCK'
  ;


ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


